package app.matiaslev.foodhabits.ui.food

import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TimePicker
import app.matiaslev.foodhabits.R
import app.matiaslev.foodhabits.models.food.FoodViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.new_food_dialog_fragment.*
import org.jetbrains.anko.toast
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import javax.inject.Inject

/**
 * Created by matiaslev on 6/26/17.
 */

class NewFoodDialogFragment: DialogFragment() {

    @Inject lateinit var foodViewModel: FoodViewModel
    var selectedHour: LocalTime = LocalTime()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = inflater?.inflate(R.layout.new_food_dialog_fragment, container)
        AndroidInjection.inject(this)
        return v!!
    }

    override fun onStart() {
        super.onStart()
        setDateTextInformation()
        timePickerListener()
        addNewFoodListener()
        addCancelListener()
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT)
    }

    fun setDateTextInformation() {
        selectedHour = LocalTime(timePicker.currentHour, timePicker.currentMinute)
    }

    fun addNewFoodListener() {
        buttonAddNewFood.setOnClickListener {
            if (editTextName.text.isNotEmpty()) {
                foodViewModel.addNewFood(selectedHour,
                        editTextName.text.toString(), getCalendarDate())
                (activity as NewFoodInterface).notifyChange()
                dismiss()
            } else {
                toast(getString(R.string.tell_us_what_you_ate))
            }

        }
    }

    fun timePickerListener() {
        timePicker.setOnTimeChangedListener({ timePicker: TimePicker, hour: Int, minutes: Int ->
            selectedHour =  LocalTime(timePicker.currentHour, timePicker.currentMinute)
        })
    }

    fun getCalendarDate(): LocalDate {
        return arguments.getSerializable("mediumDate") as LocalDate
    }

    fun addCancelListener() {
        buttonCancel.setOnClickListener { dismiss() }
    }

    interface NewFoodInterface {
        fun notifyChange()
    }

}
