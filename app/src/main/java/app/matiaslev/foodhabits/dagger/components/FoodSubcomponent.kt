package app.matiaslev.foodhabits.dagger.components

import app.matiaslev.foodhabits.ui.food.FoodActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector


/**
 * Created by matiaslev on 6/18/17.
 */

@Subcomponent
interface FoodSubcomponent : AndroidInjector<FoodActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<FoodActivity>()
}