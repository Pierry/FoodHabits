package app.matiaslev.foodhabits.dagger

import app.matiaslev.foodhabits.App
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector

/**
 * Created by matiaslev on 6/17/17.
 */

/**
 * Application component refers to application level modules only
 */

@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class,
        BuildersModule::class
)) interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}