package app.matiaslev.foodhabits.models.food

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import javax.inject.Inject

/**
 * Created by matiaslev on 6/22/17.
 */

class FoodViewModel @Inject constructor(private val foodRepository: FoodRepository) : ViewModel() {

    fun getAllFoodsFromOneDay(mediumDate: LocalDate): LiveData<List<Food>> {
        return foodRepository.getAllFoodsFromOneDay(mediumDate)
    }

    fun getFoodsToShare(mediumDate: LocalDate): List<Food> {
        return foodRepository.getFoodsToShare(mediumDate)
    }

    fun addNewFood(hour: LocalTime, name: String, mediumDate: LocalDate) {
        foodRepository.addNewFood(hour, name, mediumDate)
    }

    fun removeFoods() {
        foodRepository.removeFoods()
    }

    fun deleteFood(food: Food) {
        foodRepository.deleteOneFood(food)
    }

}