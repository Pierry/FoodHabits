package app.matiaslev.foodhabits.models.food

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.joda.time.LocalDate
import org.joda.time.LocalTime

/**
 * Created by matiaslev on 6/11/17.
 */

@Entity(tableName = "foods")
data class Food (
        @PrimaryKey(autoGenerate = true) var id : Int = 0,
        var name: String = "",
        var hour: LocalTime = LocalTime(),
        var date: LocalDate = LocalDate()
) {
    override fun toString(): String {
        return "name: $name\ndate: $date\nhour: $hour\n\n"
    }
}