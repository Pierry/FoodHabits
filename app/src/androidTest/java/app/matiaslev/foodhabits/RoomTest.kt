package app.matiaslev.foodhabits

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import app.matiaslev.foodhabits.database.AppDatabase
import app.matiaslev.foodhabits.models.food.Food
import app.matiaslev.foodhabits.models.food.FoodDao
import org.hamcrest.CoreMatchers.equalTo
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


/**
 * Created by nanlabs on 12/11/17.
 */

@RunWith(AndroidJUnit4::class)
class RoomTest {
    private lateinit var foodDao: FoodDao
    private lateinit var mDb: AppDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getTargetContext()
        mDb = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        foodDao = mDb.foodDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        mDb.close()
    }

    @Test
    @Throws(Exception::class)
    fun getAllFoods() {
        val food = Food(0,"matias", LocalTime(), LocalDate())
        foodDao.insertOneFood(food)
        assertThat(foodDao.getAllFoods().blockingObserve()!!.size, equalTo(1))
    }

    @Test
    @Throws(Exception::class)
    fun getAllFoodsFromOneDay() {
        val food = Food(0,"matias", LocalTime(), LocalDate())
        val foodForAnotherDay = Food(0,"matias", LocalTime(), LocalDate(2011, 1, 1))
        foodDao.insertOneFood(food)
        foodDao.insertOneFood(foodForAnotherDay)
        assertThat(foodDao.getAllFoodsFromOneDay(LocalDate()).blockingObserve()!!.size, equalTo(1))
    }

    @Test
    @Throws(Exception::class)
    fun getAllFoodsFromOneDayInOrder() {
        val firstFood = Food(0,"firstFood", LocalTime(9, 20), LocalDate())
        val secondFood = Food(0,"secondFood", LocalTime(12, 45), LocalDate())
        val thirdFood = Food(0,"thirdFood", LocalTime(21, 50), LocalDate())
        foodDao.insertOneFood(secondFood)
        foodDao.insertOneFood(thirdFood)
        foodDao.insertOneFood(firstFood)
        assertThat(foodDao.getAllFoodsFromOneDay(LocalDate()).blockingObserve()!!.size, equalTo(3))
        assertThat(foodDao.getAllFoodsFromOneDay(LocalDate()).blockingObserve()!![0].name,
                equalTo("firstFood"))
        assertThat(foodDao.getAllFoodsFromOneDay(LocalDate()).blockingObserve()!![1].name,
                equalTo("secondFood"))
        assertThat(foodDao.getAllFoodsFromOneDay(LocalDate()).blockingObserve()!![2].name,
                equalTo("thirdFood"))
    }


}